export default {
  Query: {
    getCoverage: (root, args, { DevAPI }) => {
      return DevAPI.get("acct", `coverages/${args.coverageUuid}`);
    }
  }
};
