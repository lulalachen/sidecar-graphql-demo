import * as R from "ramda";

export default {
  Query: {
    getHealthQuestions: async (root, args, { DevAPI }) => {
      return DevAPI.get("acct", "healthQuestions", args);
    }
  }
};
