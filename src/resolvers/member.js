import * as R from "ramda";

export default {
  Query: {
    getMember: async (root, args, { DevAPI }) => {
      return DevAPI.get("admin", `members/${args.memberUuid}`);
    },
    allMembers: async (root, args, { DevAPI }) => {
      return DevAPI.get("admin", "members", args);
    },
    getMemberSearchFilter: (_, __, { DevAPI }) =>
      DevAPI.get("admin", "members/filters")
  },
  Member: {
    stuff: () => "thing",
    fullName: root => `${root.firstName} ${root.lastName}`
  }
};
