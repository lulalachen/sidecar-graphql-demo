import * as R from "ramda";

export default {
  Query: {
    getAccount: async (root, args, { DevAPI }) => {
      return DevAPI.get("acct", `accounts/${args.accountUuid}`);
    },
    allAccounts: async (root, args, { DevAPI }) => {
      return DevAPI.get("admin", "accounts", args);
    },
    getAccountSearchFilter: (_, __, { DevAPI }) =>
      DevAPI.get("admin", "accounts/filters")
  }
};
