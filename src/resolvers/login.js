export default {
  Mutation: {
    login: (root, { type, email, password }, { DevAPI }) => {
      return DevAPI.post(
        "auth",
        `login?type=${type}`,
        {},
        {
          Authorization: `Basic ${Buffer.from(`${email}:${password}`).toString(
            "base64"
          )}`
        }
      );
    }
  }
};
