import * as R from "ramda";
import fetch from "node-fetch";
import { UserInputError } from "apollo-server";

const handleResponse = async response => {
  if (response.status >= 400) {
    const result = await response.json();
    throw new UserInputError(null, result);
  } else {
    return response.json();
  }
};

const makeQueryParams = R.ifElse(
  R.isEmpty,
  R.always(""),
  R.pipe(
    R.toPairs,
    R.filter(([__, v]) => !!v),
    R.map(R.join("=")),
    R.join("&"),
    queryParams => `?${queryParams}`
  )
);

export class DevAPI {
  constructor(token) {
    this.token = token;
    this.baseURL = "https://dev-api.sidecarhealth.com";
  }

  async get(service, source, query = {}) {
    return fetch(
      `${this.baseURL}/${service}/v1/${source}${makeQueryParams(query)}`,
      {
        method: "get",
        headers: {
          "Content-Type": "application/json",
          "x-api-key": "oI5tqK1SFkbSIO2GVnamiiNaC",
          token: this.token
        }
      }
    ).then(handleResponse);
  }

  async post(service, source, body, headers) {
    return fetch(`${this.baseURL}/${service}/v1/${source}`, {
      method: "post",
      headers: {
        "Content-Type": "application/json",
        "x-api-key": "oI5tqK1SFkbSIO2GVnamiiNaC",
        token: this.token,
        ...headers
      },
      body: JSON.stringify(body)
    }).then(handleResponse);
  }

  async patch(service, source, body) {
    return fetch(`${this.baseURL}/${service}/v1/${source}`, {
      method: "patch",
      headers: {
        "Content-Type": "application/json",
        "x-api-key": "oI5tqK1SFkbSIO2GVnamiiNaC",
        token: this.token
      },
      body: JSON.stringify(body)
    }).then(handleResponse);
  }

  async delete(service, source) {
    return fetch(`${this.baseURL}/${service}/v1/${source}`, {
      method: "delete",
      headers: {
        "Content-Type": "application/json",
        "x-api-key": "oI5tqK1SFkbSIO2GVnamiiNaC",
        token: this.token
      }
    }).then(handleResponse);
  }
}
