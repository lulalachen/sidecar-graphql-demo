import { ApolloServer } from "apollo-server";
import * as R from "ramda";
import path from "path";
import { fileLoader, mergeTypes, mergeResolvers } from "merge-graphql-schemas";

import { DevAPI } from "./datasource";

const typeDefs = mergeTypes(fileLoader(path.join(__dirname, "./schema")));
const resolvers = mergeResolvers(
  fileLoader(path.join(__dirname, "./resolvers"))
);

const server = new ApolloServer({
  introspection: true,
  typeDefs,
  resolvers,
  logger: { log: console.log },
  formatError: error => {
    const errorMessage = R.tryCatch(
      R.pipe(
        R.path(["extensions", "exception", "errors"]),
        R.join(", ")
      ),
      () => error
    )(error);
    console.log({
      error: errorMessage
    });
    return errorMessage;
  },
  formatResponse: response => {
    console.log({
      response
    });
    return response;
  },
  context: ({ req }) => {
    const token = req.headers.token || "";
    return {
      DevAPI: new DevAPI(token)
    };
  }
});

server.listen({ port: process.env.PORT || "5000" }).then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});
