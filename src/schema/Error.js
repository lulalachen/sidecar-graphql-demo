export default `
  type Error {
    message: String
    errors: [String]
  }
`;
