"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _default = {
  Query: {
    getCoverage: function getCoverage(root, args, _ref) {
      var DevAPI = _ref.DevAPI;
      return DevAPI.get("acct", "coverages/".concat(args.coverageUuid));
    }
  },
  Coverage: {
    account: function () {
      var _account = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(root, args, _ref2) {
        var DevAPI, accountUuid;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                DevAPI = _ref2.DevAPI;
                accountUuid = R.path(["account", "uuid"])(root);
                return _context.abrupt("return", DevAPI.get("acct", "accounts/".concat(accountUuid)));

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function account(_x, _x2, _x3) {
        return _account.apply(this, arguments);
      };
    }(),
    members: function members(root, _ref3, context) {
      var _ref3$where = _ref3.where,
          where = _ref3$where === void 0 ? {} : _ref3$where;
      var conditions = R.pipe(R.mapObjIndexed(function (value, key) {
        return R.propEq(key, value);
      }), R.values)(where);
      return R.pipe(R.propOr([], "members"), R.filter(R.allPass(conditions)))(root);
    }
  }
};
exports.default = _default;