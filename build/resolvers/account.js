"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var R = _interopRequireWildcard(require("ramda"));

var _default = {
  Query: {
    getAccount: function () {
      var _getAccount = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(root, args, _ref) {
        var DevAPI;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                DevAPI = _ref.DevAPI;
                return _context.abrupt("return", DevAPI.get("acct", "accounts/".concat(args.accountUuid)));

              case 2:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function getAccount(_x, _x2, _x3) {
        return _getAccount.apply(this, arguments);
      };
    }(),
    allAccounts: function () {
      var _allAccounts = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(root, args, _ref2) {
        var DevAPI;
        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                DevAPI = _ref2.DevAPI;
                return _context2.abrupt("return", !!args.keyword ? DevAPI.get("admin", "accounts/suggest", args) : DevAPI.get("admin", "accounts", args));

              case 2:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      return function allAccounts(_x4, _x5, _x6) {
        return _allAccounts.apply(this, arguments);
      };
    }(),
    getAccountSearchFilter: function getAccountSearchFilter(_, __, _ref3) {
      var DevAPI = _ref3.DevAPI;
      return DevAPI.get("admin", "accounts/filters");
    }
  }
};
exports.default = _default;