"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var R = _interopRequireWildcard(require("ramda"));

var _default = {
  Query: {
    getHealthQuestions: function () {
      var _getHealthQuestions = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(root, args, _ref) {
        var DevAPI;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                DevAPI = _ref.DevAPI;
                return _context.abrupt("return", DevAPI.get("acct", "healthQuestions", args));

              case 2:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function getHealthQuestions(_x, _x2, _x3) {
        return _getHealthQuestions.apply(this, arguments);
      };
    }()
  }
};
exports.default = _default;