"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {
  Mutation: {
    login: function login(root, _ref, _ref2) {
      var type = _ref.type,
          email = _ref.email,
          password = _ref.password;
      var DevAPI = _ref2.DevAPI;
      return DevAPI.post("auth", "login?type=".concat(type), {}, {
        Authorization: "Basic ".concat(Buffer.from("".concat(email, ":").concat(password)).toString("base64"))
      });
    }
  }
};
exports.default = _default;