"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = "\n  type Member {\n    id: ID!\n    uuid: ID!\n    status: String\n\n    firstName: String\n    lastName: String\n    email: String\n    mailingAddress: Address\n    phoneNumber: String\n    phoneVerified: Boolean\n    gender: String\n    dateOfBirth: String\n    ssnLast4: String\n\n    primary: Boolean\n    active: Boolean\n    relationWithPrimary: String\n    primaryMemberId: ID!\n\n    medicalHistory: MedicalHistory\n    deductible: Float\n    remainingDeductible: Float\n    maxAnnualAllowance: Float\n    remainingMaxAnnualAllowance: Float\n  }\n";
exports.default = _default;