"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = "\n  type Address {\n    id: ID!\n    street: String\n    city: String\n    state: String\n    zip: String\n    lon: Float\n    lat : Float\n  }\n";
exports.default = _default;