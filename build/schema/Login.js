"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = "\n  enum LoginType {\n    password\n    fingerprint\n    admin\n  }\n\n  type LoginResponse {\n    token: String\n    firstName: String\n    lastName: String\n    primary: Boolean\n  }\n\n  type Mutation {\n    login(type: LoginType!, email: String, password: String): LoginResponse\n  }\n";
exports.default = _default;