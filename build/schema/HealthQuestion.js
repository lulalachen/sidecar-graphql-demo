"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = "\n  type Answer {\n    uuid: ID!\n    text: String\n    permanent: Boolean\n  }\n\n  type HealthQuestion {\n    uuid: ID!\n    text: String\n    answer: [Answer]\n  }\n\n  type Query {\n    getHealthQuestions(stateCode: String, memberUuid: String): [HealthQuestion]\n  }\n";
exports.default = _default;