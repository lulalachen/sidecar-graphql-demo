"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = "\n  type AccountSearchFilter {\n    id: ID\n    category: String\n    type: String\n    field: String\n    op: String\n    api: String\n    vals: String\n    label: String\n  }\n  \n  type Query {\n    getAccountSearchFilter: [AccountSearchFilter]\n  }\n";
exports.default = _default;