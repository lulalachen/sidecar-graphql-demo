"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = "\n  enum PaymentSchedule {\n    MONTHLY\n    QUATARLY\n    SEMIYEARLY\n    YEARLY\n  }\n\n  type Payment {\n    uuid: ID!\n    cost: Float\n    paymentSchedule: PaymentSchedule\n    discountAmount: Float\n    discountPercentage: Float\n  }\n";
exports.default = _default;