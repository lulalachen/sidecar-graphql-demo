"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = "\n  type MedicalHistory {\n    uuid: ID!\n    procedure: String\n    prescriptions: [Prescription]\n    answers: [MedicalHistoryAnswer]\n  }\n\n  type MedicalHistoryAnswer {\n    uuid: String\n    text: String\n    permanent: Boolean\n  }\n";
exports.default = _default;