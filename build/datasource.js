"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DevAPI = void 0;

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var R = _interopRequireWildcard(require("ramda"));

var _nodeFetch = _interopRequireDefault(require("node-fetch"));

var _apolloServer = require("apollo-server");

var handleResponse =
/*#__PURE__*/
function () {
  var _ref = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(response) {
    var result;
    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            if (!(response.status >= 400)) {
              _context.next = 7;
              break;
            }

            _context.next = 3;
            return response.json();

          case 3:
            result = _context.sent;
            throw new _apolloServer.UserInputError(null, result);

          case 7:
            return _context.abrupt("return", response.json());

          case 8:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function handleResponse(_x) {
    return _ref.apply(this, arguments);
  };
}();

var makeQueryParams = R.ifElse(R.isEmpty, R.always(""), R.pipe(R.toPairs, R.filter(function (_ref2) {
  var _ref3 = (0, _slicedToArray2.default)(_ref2, 2),
      __ = _ref3[0],
      v = _ref3[1];

  return !!v;
}), R.map(R.join("=")), R.join("&"), function (queryParams) {
  return "?".concat(queryParams);
}));

var DevAPI =
/*#__PURE__*/
function () {
  function DevAPI(token) {
    (0, _classCallCheck2.default)(this, DevAPI);
    this.token = token;
    this.baseURL = "https://dev-api.sidecarhealth.com";
  }

  (0, _createClass2.default)(DevAPI, [{
    key: "get",
    value: function () {
      var _get = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(service, source) {
        var query,
            _args2 = arguments;
        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                query = _args2.length > 2 && _args2[2] !== undefined ? _args2[2] : {};
                return _context2.abrupt("return", (0, _nodeFetch.default)("".concat(this.baseURL, "/").concat(service, "/v1/").concat(source).concat(makeQueryParams(query)), {
                  method: "get",
                  headers: {
                    "Content-Type": "application/json",
                    token: this.token
                  }
                }).then(handleResponse));

              case 2:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      return function get(_x2, _x3) {
        return _get.apply(this, arguments);
      };
    }()
  }, {
    key: "post",
    value: function () {
      var _post = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee3(service, source, body, headers) {
        return _regenerator.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                return _context3.abrupt("return", (0, _nodeFetch.default)("".concat(this.baseURL, "/").concat(service, "/v1/").concat(source), {
                  method: "post",
                  headers: (0, _objectSpread2.default)({
                    "Content-Type": "application/json",
                    token: this.token
                  }, headers),
                  body: JSON.stringify(body)
                }).then(handleResponse));

              case 1:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      return function post(_x4, _x5, _x6, _x7) {
        return _post.apply(this, arguments);
      };
    }()
  }, {
    key: "patch",
    value: function () {
      var _patch = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee4(service, source, body) {
        return _regenerator.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                return _context4.abrupt("return", (0, _nodeFetch.default)("".concat(this.baseURL, "/").concat(service, "/v1/").concat(source), {
                  method: "patch",
                  headers: {
                    "Content-Type": "application/json",
                    token: this.token
                  },
                  body: JSON.stringify(body)
                }).then(handleResponse));

              case 1:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      return function patch(_x8, _x9, _x10) {
        return _patch.apply(this, arguments);
      };
    }()
  }, {
    key: "delete",
    value: function () {
      var _delete2 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee5(service, source) {
        return _regenerator.default.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                return _context5.abrupt("return", (0, _nodeFetch.default)("".concat(this.baseURL, "/").concat(service, "/v1/").concat(source), {
                  method: "delete",
                  headers: {
                    "Content-Type": "application/json",
                    token: this.token
                  }
                }).then(handleResponse));

              case 1:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      return function _delete(_x11, _x12) {
        return _delete2.apply(this, arguments);
      };
    }()
  }]);
  return DevAPI;
}();

exports.DevAPI = DevAPI;