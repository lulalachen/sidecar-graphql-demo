"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _apolloServer = require("apollo-server");

var R = _interopRequireWildcard(require("ramda"));

var _path = _interopRequireDefault(require("path"));

var _mergeGraphqlSchemas = require("merge-graphql-schemas");

var _datasource = require("./datasource");

var typeDefs = (0, _mergeGraphqlSchemas.mergeTypes)((0, _mergeGraphqlSchemas.fileLoader)(_path.default.join(__dirname, "./schema")));
var resolvers = (0, _mergeGraphqlSchemas.mergeResolvers)((0, _mergeGraphqlSchemas.fileLoader)(_path.default.join(__dirname, "./resolvers")));
var server = new _apolloServer.ApolloServer({
  introspection: true,
  typeDefs: typeDefs,
  resolvers: resolvers,
  logger: {
    log: console.log
  },
  formatError: function formatError(error) {
    var errorMessage = R.tryCatch(R.pipe(R.path(["extensions", "exception", "errors"]), R.join(", ")), function () {
      return error;
    })(error);
    console.log({
      error: errorMessage
    });
    return errorMessage;
  },
  formatResponse: function formatResponse(response) {
    console.log({
      response: response
    });
    return response;
  },
  context: function context(_ref) {
    var req = _ref.req;
    var token = req.headers.token || "";
    return {
      DevAPI: new _datasource.DevAPI(token)
    };
  }
});
server.listen({
  port: process.env.PORT || "5000"
}).then(function (_ref2) {
  var url = _ref2.url;
  console.log("\uD83D\uDE80  Server ready at ".concat(url));
});